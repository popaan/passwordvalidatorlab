
package ca.sheridancollege.dancye;

import java.util.ArrayList;
import java.util.List;
/**
 * A class that is meant to be used for practicing the skills necessary to branch/merge and pull/push
 * to a Git repository.
 * For ICE 4, please add one class that is named after your Capstone Group
 * and add one instance of that class to my ArrayList. If you wish to override the 
 * toString method in your new class, you may.
 * @author Liz Dancy, 2017.
 *
 */
public class ClassList {

	public static void main(String[] args) 
	{
		List<Person> classList = new ArrayList<Person>();
		classList.add(new Person("Student", "Derek Schunicke"));
		classList.add(new ACKDesign("Student", "Eddy"));
		classList.add(new ACKDesign("Student", "Mike"));
		classList.add(new ACKDesign("Student", "Peter"));
		
		classList.add(new MyGroup("Members", "Eddy, Mike, Peter"));

		classList.add(new Person("Instructor", "Liz Dancy"));
		classList.add(new MyGroup("Members", "your members here"));
		classList.add(new Person("Instructor2 ", "Liz"));

		classList.add(new BitToBit("Student", " Alex Matheson"));
		classList.add(new BitToBit("Student", " Kostiantyn Iakymov"));
		classList.add(new MyGroup("Members", "Alex Matheson, Konstiantyn Iakymov, Kawish Sarfaraz"));
		classList.add(new GRated("Members","G Rated","Sam Pennells","Tanvi Pathak","Laura Martinez"));
		classList.add(new MyGroup("Members", "Alex Matheson, Konstiantyn Iakymov, Kawish Sarfaraz"));	
		classList.add(new BitForce("Joel Grosjean", "Business Analyst"));
		classList.add(new BitForce("Kanishk Sahni", "Project Coordinator"));
		classList.add(new BitForce("Clayton Fernandes", "Developer"));
		classList.add(new MyGroup("Members", "Kanishk Sahni, Joel Grosjean, Clayton Fernandes"));

		classList.add(new BitToBit("Student", " Alex Matheson"));
		classList.add(new BitToBit("Student", " Kostiantyn Iakymov"));
		classList.add(new MyGroup("Members", "Alex Matheson, Konstiantyn Iakymov, Kawish Sarfaraz"));
		classList.add(new GRated("Members","G Rated","Sam Pennells","Tanvi Pathak","Laura Martinez"));
		classList.add(new MyGroup("Members", "Alex Matheson, Konstiantyn Iakymov, Kawish Sarfaraz"));	
		classList.add(new BitForce("Joel Grosjean", "Business Analyst"));
		classList.add(new BitForce("Kanishk Sahni", "Project Coordinator"));
		classList.add(new BitForce("Clayton Fernandes", "Developer"));

		classList.add(new MyGroup("Members", "Kanishk Sahni, Joel Grosjean, Clayton Fernandes"));
		classList.add(new Person("Team Lead","Darren"));
		classList.add(new acmeCorp("Acme","Daren,Kevin,Jainesh"));
		classList.add(new Person("Instructor","Ramses Trejo"));
		classList.add(new Person("Student", "Yiyao Zhang"));
		classList.add(new Person("Student","Kanghyun Kim"));
		classList.add(new Person("Student","Ying Chen"));
		classList.add(new Person("Student","Mert Havza"));
		System.out.println(classList);


	}

}

